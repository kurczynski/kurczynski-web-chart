# kurczynski-web-chart

[![Artifact Hub](https://img.shields.io/endpoint?url=https://artifacthub.io/badge/repository/kurczynski)](https://artifacthub.io/packages/search?repo=kurczynski)

Helm chart for [kurczynski-web](https://gitlab.com/kurczynski/kurczynski-web).

# Build chart

## Build signed package

```shell
make signed-package
```

### Build signed package and push

Pushes chart to `oci://docker.io/kurczynski`

```shell
make pack-push
```

# Deploy website

## New deploy

```shell
helm install kurczynski-web oci://docker.io/kurczynski/kurczynski-web --version [CHART VERSION]
```

## Upgrade existing deploy

```shell
helm upgrade [EXISTING RELEASE NAME] oci://docker.io/kurczynski/kurczynski-web --version [CHART VERSION]
```