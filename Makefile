CHART_NAME = kurczynski-web
OCI_URI = oci://docker.io/kurczynski

KEYRING_KBX = ./helm.kbx
KEYRING_GPG = ./helm.gpg
KEY_NAME = dev@kurczynski.org
KEY_FILE = private-key

GPG_ARGS = --no-default-keyring --keyring $(KEYRING_KBX)

clean:
	rm -rf \
		$(KEYRING_KBX) \
		$(KEYRING_KBX)~ \
		$(KEYRING_GPG) \
		$(CHART_NAME)-*.tgz \
		$(CHART_NAME)-*.tgz.prov

keystore-setup:
	gpg $(GPG_ARGS) --import $(KEY_FILE)
	# Helm needs to use the old GPG keyring type
	gpg $(GPG_ARGS) --export-secret-keys > $(KEYRING_GPG)

signed-package: keystore-setup
	helm package --sign --key '$(KEY_NAME)' --keyring $(KEYRING_GPG) .

pack-push: signed-package
	helm push $(CHART_NAME)-*.tgz $(OCI_URI)
